import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { VacanciesPage } from '../vacancies/vacancies';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html',
  entryComponents: [
    VacanciesPage
  ]
})
export class HomePage {

  constructor(public navCtrl: NavController) {
  }

  goToVacanciesPage() {
  	this.navCtrl.push(VacanciesPage);
  }

}
