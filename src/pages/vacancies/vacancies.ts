import { Component } from '@angular/core';
import { NavController } from 'ionic-angular';

import { ScoreInterviewsPage } from '../score-interviews/score-interviews';

@Component({
  selector: 'page-vacancies',
  templateUrl: 'vacancies.html',
  entryComponents: [
    ScoreInterviewsPage
  ]
})
export class VacanciesPage {

  constructor(public navCtrl: NavController) {
  }

  ionViewDidLoad() {
  }

  goToScoreInterviewsPage() {
  	this.navCtrl.push(ScoreInterviewsPage);
  }

  goBack() {
    this.navCtrl.pop();
  }

}
  
