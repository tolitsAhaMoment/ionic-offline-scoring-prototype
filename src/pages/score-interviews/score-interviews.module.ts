import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ScoreInterviewsPage } from './score-interviews';

@NgModule({
  declarations: [
    ScoreInterviewsPage,
  ],
  imports: [
    IonicPageModule.forChild(ScoreInterviewsPage),
  ],
  exports: [
    ScoreInterviewsPage
  ]
})
export class ScoreInterviewsPageModule {}
